from flask import *

app = Flask(__name__)

@app.route('/saludar')
def saludar():
    return('Hola mundo')

@app.route('/despedir')
def despedir():
    return('Adio mundo cruel')

@app.route('/sumar', methods=['POST'])
def sumar():
    misdatos = request.get_json()
    a = misdatos['numero1']
    b = misdatos['numero2']
    resultado = a + b
    return jsonify({
        "resultado": resultado
    })

@app.route('/restar', methods=['POST'])
def restar():
    misdatos = request.get_json()
    a = misdatos['numero1']
    b = misdatos['numero2']
    resultado = a - b
    return jsonify({
        "resultado": resultado
    })

@app.route('/multiplicar', methods=['POST'])
def multiplicar():
    misdatos = request.get_json()
    a = misdatos['numero1']
    b = misdatos['numero2']
    resultado = a * b
    return jsonify({
        "resultado": resultado
    })

@app.route('/dividir', methods=['POST'])
def dividir():
    misdatos = request.get_json()
    a = misdatos['numero1']
    b = misdatos['numero2']
    resultado = a / b
    return jsonify({
        "resultado": resultado
    })

@app.route('/modulo', methods=['POST'])
def modulo():
    misdatos = request.get_json()
    a = misdatos['numero1']
    b = misdatos['numero2']
    resultado = a % b
    return jsonify({
        "resultado": resultado
    })

if __name__ == '__main__':
    app.run()